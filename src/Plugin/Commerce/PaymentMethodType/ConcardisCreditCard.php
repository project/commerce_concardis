<?php

namespace Drupal\commerce_concardis\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\CreditCard;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the Concardis credit card payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "concardis_credit_card",
 *   label = @Translation("Concardis credit card"),
 * )
 */
class ConcardisCreditCard extends CreditCard {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['payment_instrument_id'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Concardis payment instrument ID'))
      ->setDescription(t('The payment instrument id.'))
      ->setRequired(TRUE);

    $fields['merchant_payment_instrument_id'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Merchant payment instrument ID'))
      ->setDescription(t('The marchant payment instrument id.'));

    $fields['concardis_product'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Concardis product type ID'))
      ->setDescription(t('The Concardis product type id.'));

    $fields['recurring'] = BundleFieldDefinition::create('boolean')
      ->setLabel(t('Card can be used for recurring payments'))
      ->setDescription(t('Card can be used for recurring payments.'));

    return $fields;
  }

  public function buildLabel(PaymentMethodInterface $payment_method) {
    return parent::buildLabel($payment_method);
  }

}
