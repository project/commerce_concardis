<?php

namespace Drupal\commerce_concardis\Plugin\Commerce\PaymentGateway;

use CommerceGuys\Intl\Currency\CurrencyRepositoryInterface;
use Concardis\Payengine\Lib\Internal\Config\MerchantConfiguration;
use Concardis\Payengine\Lib\Internal\Exception\PayengineResourceException;
use Concardis\Payengine\Lib\Models\Request\Customer as CustomerRequest;
use Concardis\Payengine\Lib\Models\Request\Customers\Address as AddressRequest;
use Concardis\Payengine\Lib\Models\Request\Orders\Item as OrderItemRequest;
use Concardis\Payengine\Lib\Models\Response\Orders\Transaction;
use Concardis\Payengine\Lib\Payengine;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethod;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the HostedFields payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "concardis_widget_inline",
 *   label = "Concardis (Inline Widget)",
 *   display_label = "Concardis Payengine",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_concardis\PluginForm\InlineWidget\PaymentMethodAddForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Concardis extends OffsitePaymentGatewayBase implements SupportsStoredPaymentMethodsInterface, SupportsRefundsInterface {

  /**
   * @var \Concardis\Payengine\Lib\Payengine
   */
  protected $api;

  /**
   * @var \CommerceGuys\Intl\Currency\CurrencyRepositoryInterface
   */
  protected $currencyRepository;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\commerce_payment\PaymentMethodTypeManager
   */
  protected $paymentMethodTypeManager;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, CurrencyRepositoryInterface $currencyRepository, ModuleHandlerInterface $moduleHandler, LoggerChannelInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->logger = $logger;
    $this->currencyRepository = $currencyRepository;
    $this->api = $this->getApi();
    $this->paymentStorage = $entity_type_manager->getStorage('commerce_payment');
    $this->paymentMethodTypeManager = $payment_method_type_manager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * @return \Concardis\Payengine\Lib\Payengine
   */
  private function getApi() {
    $options = new MerchantConfiguration();
    $options->setApiKey($this->configuration['api_key']);
    $options->setMerchantId($this->configuration['merchant_id']);
    $options->setIsLiveMode($this->getMode() == 'live');
    // Construct API client.
    return new Payengine($options);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.currency_repository'),
      $container->get('module_handler'),
      $container->get('logger.channel.concardis')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'merchant_id' => '',
        'api_key' => '',
        'allowed_products' => [],
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];
    $form['allowed_products'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed payment methods'),
      '#options' => [
        'creditcard' => $this->t('Credit card'),
        'paypal' => $this->t('Paypal'),
        'paydirekt' => $this->t('Paydirect'),
        'ratepay-invoice' => $this->t('Ratepay Invoice'),
        'ratepay-directdebit' => $this->t('Ratepay direct debit'),
        'ratepay-installment' => $this->t('Ratepay installment'),
        'sepa' => $this->t('Sepa'),
        'sofort' => $this->t('Sofort'),
        'klarna-pay-now' => $this->t('Klarna "Pay Now"'),
        'klarna-pay-later' => $this->t('Klarna "Pay Later"'),
        'klarna-slice-it' => $this->t('Klarna "Slice it"'),
        'prepayment' => $this->t('Prepayment'),
        'giropay' => $this->t('Giropay'),
        'eps' => $this->t('EPS'),
        'ideal' => $this->t('Ideal'),
        'alipay' => $this->t('Alipay'),
        'bancontact' => $this->t('Bancontact'),
        'wechat-pay' => $this->t('Wechat Pay'),
      ],
      '#default_value' => $this->configuration['allowed_products'],
      '#description' => $this->t('Check first which methods are enabled in <a target="_blank" href="https://merchant.payengine.de/#!/u/0/m/0/payment_methods">Merchant Dashboard</a>. If you select other payment methods than in Merchant Dashboard they will not appear in payment step, but you can limit the allowed methods by enabling only the ones you need.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    if (empty($form_state->getErrors())) {
      try {
        $values = $form_state->getValues();
        $configuration = $values['configuration'][$values['plugin']];
        $options = new MerchantConfiguration();
        $options->setApiKey($configuration['api_key']);
        $options->setMerchantId($configuration['merchant_id']);
        $options->setIsLiveMode($this->getMode() == 'live');
        // Construct API client.
        $api = new Payengine($options);
        $api->paymentinstruments()->get();
      } catch (PayengineResourceException $exception) {
        $form_state->setError($form['merchant_id'], $exception->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['allowed_products'] = $values['allowed_products'];
    }
  }

  /**
   * Get order from Concardis payment gateway.
   *
   * @param string $order_id
   *   Order Id.
   *
   * @return \Concardis\Payengine\Lib\Models\Response\ListWrapper|\Concardis\Payengine\Lib\Models\Response\Order|false
   *   Order object or FALSE if order doesn't exist.
   */
  public function getOrder($order_id) {
    $remote_order = FALSE;
    try {
      $remote_order = $this->api->orders($order_id)->get();
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    } catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }
    return $remote_order;
  }

  /**
   * Create order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return \Concardis\Payengine\Lib\Models\Response\Order|false
   * @see https://docs.payengine.de/buildyourown/apidocs#operation/getOrders
   */
  public function createOrder(OrderInterface $order) {
    $remote_order = FALSE;
    try {
      $data = $this->prepareOrderData($order);
      $remote_order = $this->api->orders()->post($data);
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    }
    return $remote_order;
  }

  /**
   * @param \Concardis\Payengine\Lib\Internal\Exception\PayengineResourceException $exception
   */
  private function handleException(PayengineResourceException $exception) {
    $this->messenger()->addError($exception->getMessage());
    $response = Json::decode($exception->getResponseBody());
    if (!empty($response['errors'])) {
      $errors = [];
      foreach ($response['errors'] as $error) {
        $errors[] = $error['message'];
      }
      $this->logger->error(implode('. ', $errors));
    } else {
      $this->logger->error($exception->getMessage());
    }
  }

  /**
   * @param $order
   *
   * @return array
   * @throws \Exception
   */
  private function prepareOrderData(OrderInterface $order) {
    $data = [
      'initialAmount' => $this->minorUnitsConverter->toMinorUnits($order->getTotalPrice()),
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
      'transactionType' => 'DEBIT',
      'async' => [
        'successUrl' => Url::fromRoute('commerce_payment.checkout.return', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'failureUrl' => Url::fromRoute('commerce_payment.checkout.cancel', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'cancelUrl' => Url::fromRoute('commerce_payment.checkout.cancel', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'notifications' => [
          [
            'notificationUrn' => $this->getNotifyUrl()->setAbsolute()->toString(),
            'notificationState' => ['updated'],
          ],
        ],
      ],
      'merchantOrderId' => $order->id(),
      'terms' => $this->time->getRequestTime(),
      'privacy' => $this->time->getRequestTime(),
    ];
    $allowed_products = array_filter($this->configuration['allowed_products']);
    if (!empty($allowed_products)) {
      $data['allowedProducts'] = array_values($allowed_products);
    }
    $customer = $this->createCustomer($order->getEmail());
    if ($customer) {
      $data['customer'] = $customer->getCustomerId();
      $address = $this->createAddress($data['customer'], $order->getBillingProfile()
        ->get('address')
        ->getValue());
      if ($address) {
        $data['billingAddress'] = $address->getAddressId();
      }
    }
    // Create order items.
    foreach ($order->getItems() as $item) {
      $data['basket'][] = $this->createOrderItem($item);
    }
    $this->moduleHandler->alter('concardis_order_data', $data, $order);
    return $data;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $orderItem
   *
   * @return array
   */
  private function createOrderItem(OrderItemInterface $orderItem) {
    $tax_rate = 0;
    $tax_adjustments = array_filter($orderItem->getAdjustments(), function ($adjustment) {
      /** @var \Drupal\commerce_order\Adjustment $adjustment */
      return $adjustment->getType() == 'tax';
    });
    $unit_price_tax = $unit_price = $orderItem->getUnitPrice();
    $total_price_tax = $total_price = $orderItem->getTotalPrice();
    if (!empty($tax_adjustments)) {
      foreach ($tax_adjustments as $adjustment) {
        $tax_rate = $adjustment->getPercentage();
        if ($adjustment->isIncluded()) {
          $unit_price = $unit_price->subtract($adjustment->getAmount());
          $total_price = $total_price->subtract($adjustment->getAmount()
            ->multiply($orderItem->getQuantity()));
        } else {
          $unit_price_tax = $unit_price_tax->add($adjustment->getAmount());
          $total_price_tax = $total_price_tax->add($adjustment->getAmount()
            ->multiply($orderItem->getQuantity()));
        }
      }
      $tax_rate = round(($total_price_tax - $total_price)/$total_price * 100);
    }
    $item = new OrderItemRequest();
    $item->setQuantity((int)$orderItem->getQuantity());
    $item->setUnitPrice($this->minorUnitsConverter->toMinorUnits($unit_price));
    $item->setUnitPriceWithTax($this->minorUnitsConverter->toMinorUnits($unit_price_tax));
    $item->setTotalPriceWithTax($this->minorUnitsConverter->toMinorUnits($total_price_tax));
    $item->setTotalPrice($this->minorUnitsConverter->toMinorUnits($total_price));
    $item->setArticleNumber($orderItem->getPurchasedEntity()->getSku());
    $item->setName($orderItem->label());
    $item->setTax($tax_rate);
    return $item->__toArray();
  }

  /**
   * @param $order_id
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return mixed
   * @throws \Exception
   */
  public function updateOrder($order_id, OrderInterface $order) {
    $remote_order = FALSE;
    try {
      $data = $this->prepareOrderData($order);
      $remote_order = $this->api->orders($order_id)->patch($data);
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    }
    return $remote_order;
  }

  /**
   * Creates customer.
   *
   * @param $email
   *
   * @return \Concardis\Payengine\Lib\Models\Response\Customer|false
   */
  public function createCustomer($email) {
    $customer = FALSE;
    try {
      $customerRequest = new CustomerRequest();
      $customerRequest->setEmail($email);
      $customer = $this->api->customer()->post($customerRequest);
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    }
    return $customer;
  }

  /**
   * @param string $customer_id
   * @param array $address
   *
   * @return \Concardis\Payengine\Lib\Models\Response\Customers\Address|false
   * @throws \Exception
   */
  public function createAddress($customer_id, $address) {
    $remote_address = FALSE;
    try {
      $addressRequest = new AddressRequest();
      $addressRequest->setFirstName($address[0]['given_name']);
      $addressRequest->setLastName($address[0]['family_name']);
      $addressRequest->setZip($address[0]['postal_code']);
      $addressRequest->setStreet($address[0]['address_line1']);
      $addressRequest->setCountry($address[0]['country_code']);
      $addressRequest->setCity($address[0]['locality']);
      $addressRequest->setState($address[0]['administrative_area']);
      $remote_address = $this->api->customer($customer_id)->addresses()->post($addressRequest);
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    }
    return $remote_address;
  }

  /**
   * @param $order_id
   *
   * @return array|\Concardis\Payengine\Lib\Internal\Resource\Orders\Transactions
   * @throws \Exception
   */
  public function getOrderTransactions($order_id) {
    try {
      $transactions = $this->api->orders($order_id)->transactions()->get();
    } catch (PayengineResourceException $exception) {
      $this->handleException($exception);
    }
    return !empty($transactions) ? $transactions->getElements() : [];
  }

  /**
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param $order_remote_id
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function savePayment(PaymentInterface $payment, $order_remote_id) {
    $transactions = $this->getOrderTransactions($order_remote_id);
    /** @var \Concardis\Payengine\Lib\Models\Response\Orders\Transaction $transaction */
    foreach ($transactions as $transaction) {
      if ($transaction->getStatus() == 'SUCCESS') {
        $payment->setAmount($this->minorUnitsConverter->fromMinorUnits($transaction->getInitialAmount(), $transaction->getCurrency()));
        $authorized_at = (int) round($transaction->getCreatedAt() / 1000);
        $modified_at = (int) round($transaction->getModifiedAt() / 1000);
        $payment->setAuthorizedTime($authorized_at);
        $payment->setCompletedTime($modified_at);
        $payment->setRemoteId($transaction->getTransactionId());
        $payment->setRemoteState($transaction->getStatus());
        $product = $transaction->getOrder()->getProduct();
        $payment->setAvsResponseCode($product);
        if ($product == 'creditcard') {
          /** @var \Concardis\Payengine\Lib\Models\Response\PaymentInstrument $payment_instrument */
          $payment_instrument = $transaction->getOrder()->getPaymentInstrument();
          /** @var PaymentMethod $payment_method */
          $card_type = 'visa';
          $credit_card_types = [
            'visa',
            'mastercard',
            'maestro',
            'amex',
            'dinersclub',
            'discover',
            'jcb',
            'unionpay',
          ];
          if (\in_array(\strtolower($payment_instrument->getAttributes()['brand']), $credit_card_types)) {
            $card_type = \strtolower($payment_instrument->getAttributes()['brand']);
          }
          $order = $payment->getOrder();
          $payment_method = $this->entityTypeManager->getStorage('commerce_payment_method')->loadByProperties([
            'card_number' => \substr($payment_instrument->getAttributes()['cardNumber'], -4),
            'uid' => \Drupal::currentUser()->id(),
          ]);
          $payment_method = \reset($payment_method);
          if (!$payment_method instanceof PaymentMethodInterface) {
            $data = [
              'type' => 'concardis_credit_card',
              'billing_profile' => $order->getBillingProfile(),
              'payment_gateway' => $this->parentEntity->id(),
              'uid' => \Drupal::currentUser()->id(),
              'remote_id' => $payment_instrument->getPaymentInstrumentId(),
              'attributes' => $payment_instrument->getAttributes(),
              'instrument_type' => $payment_instrument->getType(),
              'card_type' => $card_type,
              'card_number' => \substr($payment_instrument->getAttributes()['cardNumber'], -4),
              'card_exp_month' => $payment_instrument->getAttributes()['expiryMonth'],
              'card_exp_year' => $payment_instrument->getAttributes()['expiryYear'],
            ];
            $payment_method = PaymentMethod::create($data);
            $payment_method->get('payment_instrument_id')->setValue($payment_instrument->getPaymentInstrumentId());
            $payment_method->get('merchant_payment_instrument_id')->setValue($payment_instrument->getMerchantPaymentInstrumentId());
            $payment_method->get('concardis_product')->setValue($transaction->getOrder()->getProduct());
            $payment_method->get('recurring')->setValue($payment_instrument->isRecurring());
            $payment_method->save();
          }
          $order->set('payment_method', $payment_method);
          $attributes = $payment_instrument->getAttributes();
          $label = $attributes['brand'] . ' ' . $attributes['cardNumber'];
          $payment->setAvsResponseCodeLabel($label);
        } else {
          $payment->setAvsResponseCodeLabel($transaction->getDescription());
        }
        $payment->setState('completed');
        $payment->save();
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $request_content = Json::decode($request->getContent());
    $this->logger->debug($request->getContent());
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $request_content = Json::decode($request->getContent());
    $this->logger->debug($request->getContent());
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    parent::onNotify($request);
    $this->logger->debug($request->getContent());
    /*
     * A string array containing the following keys:
     *   - referenceId
     *   - usage
     *   - status (one of SUCCESS, CANCELLED, TIMEOUT)
     *   - verificationHash
     */
//    $request_content = Json::decode($request->getContent());
//    $reference_id = $request_content['referenceId'];
//
//    $payment = $this->paymentStorage->loadByRemoteId($reference_id);
//    if (empty($payment)) {
//      throw new InvalidRequestException($this->t('No transaction found for cashpresso reference ID @reference_id.', ['@reference_id' => $reference_id]));
//    }
//    if (empty($request_content['status'])) {
//      throw new InvalidRequestException($this->t('No payment status set in success callback for cashpresso reference ID @reference_id.', ['@reference_id' => $reference_id]));
//    }
//    $verification_hash = $this->generateVerificationHash($payment, $request_content['status']);
//    if ($verification_hash != $request_content['verificationHash']) {
//      throw new InvalidRequestException($this->t('Verification hash does not match for cashpresso reference ID @reference_id.', ['@reference_id' => $reference_id]));
//    }
//
//    switch ($request_content['status']) {
//      case 'canceled':
//        $void_transition = $payment->getState()->getWorkflow()->getTransition('void');
//        $payment->getState()->applyTransition($void_transition);
//        $payment->save();
//        break;
//
//      case 'success':
//        $capture_transition = $payment->getState()->getWorkflow()->getTransition('capture');
//        $payment->getState()->applyTransition($capture_transition);
//        $payment->save();
//        break;
//
//      case 'timeout':
//        $expire_transition = $payment->getState()->getWorkflow()->getTransition('expire');
//        $payment->getState()->applyTransition($expire_transition);
//        $payment->save();
//        break;
//
//      default:
//        throw new InvalidRequestException($this->t('Invalid transaction status returned for cashpresso reference ID @reference_id.', ['@reference_id' => $reference_id]));
//    }
  }

  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // Perform the create payment request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Remember to take into account $capture when performing the request.
    $amount = $payment->getAmount();
    $payment_method_token = $payment_method->getRemoteId();
    // Perform payment.
    $order = $payment->getOrder();
    $data = [
      'initialAmount' => $this->minorUnitsConverter->toMinorUnits($order->getTotalPrice()),
      'currency' => $order->getTotalPrice()->getCurrencyCode(),
      'product' => $payment_method->concardis_product->value,
      'async' => [
        'successUrl' => Url::fromRoute('commerce_payment.checkout.return', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'failureUrl' => Url::fromRoute('commerce_payment.checkout.cancel', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'cancelUrl' => Url::fromRoute('commerce_payment.checkout.cancel', ['commerce_order' => $order->id(), 'step' => 'payment'])->setAbsolute()->toString(),
        'notifications' => [
          [
            'notificationUrn' => $this->getNotifyUrl()->setAbsolute()->toString(),
            'notificationState' => ['updated'],
          ],
        ],
      ],
      'payment' => [
        'paymentInstrumentId' => $payment_method->payment_instrument_id->value,
      ],
      'merchantOrderId' => $order->id(),
      'terms' => $this->time->getRequestTime(),
      'privacy' => $this->time->getRequestTime(),
    ];
    $this->moduleHandler->alter('concardis_order_data', $data, $order);
    try {
      /** @var \Concardis\Payengine\Lib\Models\Response\Order $order */
      $order = $this->api->orders()->debit()->post($data);
    } catch (\Exception $exception) {
      $payment->delete();
      $exception = new PaymentGatewayException($exception->getMessage());
      throw $exception;
    }

    // The remote ID returned by the request.
    $next_state = $capture ? 'completed' : 'authorization';
    $payment->setState($next_state);
    $transactions = $order->getTransactions();
    $transaction = \reset($transactions);
    if ($transaction instanceof Transaction) {
      $payment->setRemoteId($transaction->getTransactionId());
    }
    $payment->save();

    // Save the order id.
    $commerce_order = $payment->getOrder();
    $order_id = $order->getOrderId();
    if (empty($commerce_order->getData('remote_id'))) {
      $commerce_order->setData('remote_id', $order_id);
      $commerce_order->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // Refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.

    $data = [
      'initialAmount' => $this->minorUnitsConverter->toMinorUnits($amount),
      'currency' => $amount->getCurrencyCode(),
    ];
    try {
      /** @var \Concardis\Payengine\Lib\Models\Response\Order $order */
      $remote_id = $payment->getOrder()->getData('remote_id');
      $order = $this->api->orders($remote_id)->transactions($payment->getRemoteId())->refund()->post($data);
    } catch (\Exception $exception) {
      $exception = new PaymentGatewayException($exception->getMessage());
      throw $exception;
    }
    if ($order->getStatus() === 'SUCCESS') {
      $old_refunded_amount = $payment->getRefundedAmount();
      $new_refunded_amount = $old_refunded_amount->add($amount);
      if ($new_refunded_amount->lessThan($payment->getAmount())) {
        $payment->setState('partially_refunded');
      }
      else {
        $payment->setState('refunded');
      }

      $payment->setRefundedAmount($new_refunded_amount);
      $payment->save();
    }
    else {
      throw new \Exception(t("Refund failed"));
    }
  }

  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

}
