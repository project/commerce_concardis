<?php


namespace Drupal\commerce_concardis\PluginForm\InlineWidget;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class PaymentMethodAddForm
 *
 * @package Drupal\commerce_concardis\PluginForm\InlineWidget
 */
class PaymentMethodAddForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_concardis\Plugin\Commerce\PaymentGateway\Concardis $plugin */
    $plugin = $this->plugin;
    $configuration = $plugin->getConfiguration();
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    $remote_id = $order->getData('remote_id');
    if ($remote_id) {
      try {
        /** @var \Concardis\Payengine\Lib\Models\Response\Order $remote_order */
        $remote_order = $this->plugin->getOrder($remote_id);
      } catch (\Exception $exception) {
        $form_state->setError($form['concardis_order_remote_id'], $exception->getMessage());
        return $form;
      }
      // Modify order only if it is not paid yet.
      if ($remote_order->getStatus() == 'CREATED') {
        $remote_order = $this->plugin->updateOrder($remote_id, $order);
      } elseif ($remote_order->getStatus() == 'OPEN') {
        // $form_state->setError($form['concardis_order_remote_id'], 'Order was already paid');
        // This means that remote order was either paid or refunded/cancelled.
        // @TODO Should update the Drupal Commerce Order?
      } else {
        $form_state->setError($form['concardis_order_remote_id'], 'Order was cancelled');
        $remote_order = FALSE;
      }
    } else {
      $remote_order = $this->plugin->createOrder($order);
    }
    if (empty($remote_order)) {
      return $form;
    }
    if (!empty($remote_order) && is_null($remote_id)) {
      $order->setData('remote_id', $remote_order->getOrderId());
      $order->save();
    }
    $form['concardis_order_remote_id'] = [
      '#type' => 'hidden',
      '#value' => $remote_id,
    ];
    $library = 'commerce_concardis/inline_widget';
    if ($plugin->getMode() == 'test') {
      $library .= '_test';
    }
    $form['#attached']['library'][] = $library;
    $form['#attached']['drupalSettings']['concardis'] = [
      'merchantId' => $configuration['merchant_id'],
      'orderId' => $remote_order->getOrderId(),
      'language' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ];
    $form['#attributes']['class'][] = 'concardis-inline-widget-form';

    $form['concardis_checkout'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => ['id' => 'concardis-widget-container'],
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Complete purchase'),
      '#attributes' => [
        'class' => [
          'concardis-payment-submit',
        ],
      ],
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromUri($form['#cancel_url']),
      '#attributes' => [
        'class' => [
          'concardis-payment-cancel',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    if (empty($values['concardis_order_remote_id'])) {
      $form_state->setError($form['concardis_order_remote_id'], $this->t('Remote order does not exist.'));
      return;
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_concardis\Plugin\Commerce\PaymentGateway\Concardis $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $remote_id = $values['concardis_order_remote_id'];
    $success = $payment_gateway_plugin->savePayment($payment, $remote_id);
    if (!$success) {
      throw new NeedsRedirectException($form['#exception_url']);
    }
  }

}
