(function (Drupal) {

  var widgetReference;

  /**
   * Polyfill for Element.closest().
   */
  if (!Element.prototype.matches) {
    Element.prototype.matches =
      Element.prototype.msMatchesSelector ||
      Element.prototype.webkitMatchesSelector;
  }

  if (!Element.prototype.closest) {
    Element.prototype.closest = function(s) {
      var el = this;

      do {
        if (Element.prototype.matches.call(el, s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);
      return null;
    };
  }

  /**
   * Initializes PayEngine widget.
   *
   * @param error
   * @param result
   */
  function initCallback(error, result) {
    if (error) {
      const messages = new Drupal.Message();
      messages.add(error.message, {type: 'error'});
      return;
    }
    widgetReference = result;
  }

  /**
   * Responds to the transaction result.
   *
   * @param error
   * @param result
   */
  function resultCallback(error, result) {
    if (error) {
      const messages = new Drupal.Message();
      messages.add(error.message, {type: 'error'});
      return;
    }
    // Proceed further if there are no errors.
    // Hide "Cancel" button.
    document.getElementsByClassName('concardis-payment-cancel').item(0).style.display = 'none';
    // Submit the payment form.
    var container = document.getElementById("concardis-widget-container");
    container.closest('form').submit();
  }

  Drupal.behaviors.concardis_inline_wigdet = {
    attach: function (context, settings) {
      var container = document.getElementById("concardis-widget-container");
      if (container.innerHTML !== '') {
        return;
      }
      var publishableKey = settings.concardis.merchantId;
      var orderId = settings.concardis.orderId;
      var optionalParameters = new Object();
      optionalParameters.initCallback = initCallback;
      optionalParameters.language = settings.concardis.language;
      optionalParameters.hidePayButton = false;
      optionalParameters.redirectUser = true;
      optionalParameters.hideTitleIcons = false;
      optionalParameters.optionalFields = {
        autoResizeHeight: true
      };
      PayEngineWidget.initAsInlineComponent(
        container,
        publishableKey,
        orderId,
        optionalParameters,
        resultCallback
      );
    }
  };

})(Drupal);
