<?php

/**
 * @file
 * Hooks provided by the Commerce Concardis module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows modules to alter the data sent to payment gateway.
 *
 * @param array $data
 *   Data prepared by commerce_concardis module based on give order.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   Given order.
 */
function hook_concardis_order_data_alter(array &$data, \Drupal\commerce_order\Entity\OrderInterface $order) {
  if (\Drupal::moduleHandler()->moduleExists('config_split')) {
    // Add environment prefix to merchantOrderId.
    $config_split_storage = \Drupal::entityTypeManager()->getStorage('config_split');
    $active_splits = $config_split_storage->loadByProperties(['status' => 1]);
    if (!empty($active_splits)) {
      // Normally it should be only 1.
      $active_split = reset($active_splits);
      $environment = $active_split->id();
      $data['merchantOrderId'] = $environment . '_' . $data['merchantOrderId'];
    }
  }
}
